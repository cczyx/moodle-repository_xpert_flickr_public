<?php
// This file is part of the Flickr public (Xpert) repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'repository_xpert_flickr_public', language 'en', branch 'master'
 * 
 * @package    repository_xpert_flickr_public
 * @copyright  2013 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['all'] = 'All';
$string['apikey'] = 'API key';
$string['author'] = 'Copyright holder';
$string['backtosearch'] = 'Back to search screen';
$string['callbackurl'] = 'Callback URL';
$string['colour'] = 'Background and text colours';
$string['colourcombinationnotexists'] = 'The submitted colour combination does not exist';
$string['colourwhiteblack'] = 'Background: White, Text: Black';
$string['colourblackwhite'] = 'Background: Black, Text: White';
$string['colourgreyblack'] = 'Background: Light grey, Text: Black';
$string['colourbluebrown'] = 'Background: Baby blue, Text: Dark brown';
$string['colourpeachbrown'] = 'Background: Peach, Text: Dark brown';
$string['colouryellowblack'] = 'Background: Light yellow, Text: Black';
$string['colourpinkblack'] = 'Background: Light pink, Text: Black';
$string['commercialuse'] = 'I want to be able to use the images commercially';
$string['configplugin'] = 'Flickr public configuration';
$string['creativecommonscommercial'] = 'Only creative commons commercial';
$string['debug'] = 'Debug images?';
$string['debug_description'] = 'Turning debug images on will cause images to be replaced by a blue image with debug info';
$string['fulltext'] = 'Full text';
$string['information'] = '<div>Get a <a href="http://www.flickr.com/services/api/keys/">Flickr API Key</a> for your Moodle site. </div>';
$string['large'] = 'Large: 1024 x 768 Max';
$string['license'] = 'License';
$string['medium'] = 'Medium: 500 x 375 Max';
$string['modification'] = 'I want to be able to modify the images';
$string['notitle'] = 'notitle';
$string['nullphotolist'] = 'There are no photos in this account';
$string['remember'] = 'Remember me';
$string['pluginname_help'] = 'Repository on flickr.com';
$string['pluginname'] = 'Flickr public (Xpert)';
$string['secret'] = 'Secret';
$string['sharealike'] = 'Yes, as long as others share alike';
$string['small'] = 'Small: 240 x 180 Max';
$string['size'] = 'Image size';
$string['standard'] = 'Standard: 500 x 375 Max';
$string['tag'] = 'Tag';
$string['useattribution'] = 'Add license attribution to photos';
$string['xpert_flickr_public:view'] = 'Use Flickr public repository in file picker';
$string['xpert_flickr_public:debug'] = 'Debug Flickr public repository';
